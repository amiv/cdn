from flask import Flask, request, jsonify, send_file, abort
from flask_cors import CORS
from api import api
from yaml import safe_load
from os import getenv, makedirs
import os.path

with open(getenv('CONFIG', 'config.yaml')) as cfg:
  config = safe_load(cfg)

app = Flask(__name__)

app.config.update(
  API_URL=config.get('api_url', 'localhost'),
  BASE_DIR=getenv('ROOT_DIR', '/data'),
  ALLOWED_FILENAMES=config.get('allowed_filenames', ['txt']),
  DIRECTORIES=config.get('directories', [])
)

# create folders that weren't created yet
for directory in app.config['DIRECTORIES'].keys():
  d = os.path.join(app.config['BASE_DIR'], directory)
  if not os.path.isdir(d):
    try:
      makedirs(d)
    except:
      print('error creating dir ' + str(d))

CORS(app)
api.init_app(app)
