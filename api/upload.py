from flask import Flask, flash, request, redirect, url_for, current_app
from flask_restplus import Namespace, Resource

from werkzeug.utils import secure_filename
from werkzeug.exceptions import BadRequest
from werkzeug.datastructures import FileStorage

from .utils.files import generateFilename, get_mimetype
from .auth.check_token import check_auth
import os.path

import mimetypes

api = Namespace(
    'upload',
    description='Upload Files to the CDN')

upload_parser = api.parser()
upload_parser.add_argument(
    'file',
    location='files',
    type=FileStorage,
    required=True)
upload_parser.add_argument('token', required=True)

@api.route('/<directory>')
class UploadEndpoint(Resource):
    @api.expect(upload_parser)
    def post(self, directory):
        # Validate arguments
        args = upload_parser.parse_args()
        if not args.get('file'):
            BadRequest('No file submitted')
        if not args.get('token'):
            BadRequest('No token submitted')

        f = args['file']
        token = args['token']

        # check requested directory
        c = current_app.config['DIRECTORIES'].get(directory, False)
        if not c:
            raise BadRequest('Requested directory is not configured')

        # check if filename is allowed
        ext = f.filename.split('.')[-1]
        if ext not in current_app.config['ALLOWED_FILENAMES']:
            raise BadRequest('File extension not allowed')
        
        # check if MIME-Type of file matches the extension
        mimetype = get_mimetype(f)

        if ('.' + ext) not in mimetypes.guess_all_extensions(mimetype):
            raise BadRequest('Mimetype doesn\'t match to file extension')

        groups = c['group'] if type(c['group']) == list else [c['group']]

        # check authorization for requested directory
        if not check_auth(token, groups):
            raise BadRequest('Not authorized to upload to this directory')

        # save file
        save_fn = generateFilename(directory, ext)
        f.save(save_fn)
        link = '/'.join(save_fn.split('/')[1:])

        return {'message': 'Successfully saved', 'link': link}, 202
