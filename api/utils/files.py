from flask import current_app
import magic
import uuid

import os.path

def generateFilename(add_dir, extension):
  base_dir = current_app.config['BASE_DIR']
  while True:
    name = uuid.uuid1()
    filename = os.path.join(base_dir, add_dir, str(name) + '.' + extension)
    if not os.path.exists(filename):
      return filename

def get_mimetype(file):
  """Get the mimetype from file data."""
  data = file.stream.read()
  f = magic.Magic(mime=True)
  return f.from_buffer(data)